<?php



function homeone_print_embed_style($css)

{

	$primary_color = printcart_get_options('nbcore_primary_color');

	$text_link = printcart_get_options('nbcore_link_hover_color');

	$text_link_hover = printcart_get_options('nbcore_link_color');

	$css .= "

	.site-footer .footer-bot-section .widget ul li a:before {

		top: 2px;

	}

	.main-navigation .menu-main-menu-wrap #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link {

		font-weight: 600;

		font-family: Poppins;

	}

	.ib-shape .icon{                

		background-color: " . esc_attr($primary_color) . ";

	}

	.vc-blog .blog-content .hb-readmore {

		color: ". esc_attr($text_link) .";

	}

	.vc-blog .blog-content .hb-readmore:hover {

		color: ". esc_attr($text_link_hover) .";

	}

	footer.site-footer .footer-top-section .footer_top_title, .site-footer .footer-bot-section .widget ul li a, footer.site-footer p {

		color: #ccc;

	}

	.woocommerce form .form-row abbr.required {

		text-decoration: none;

		border: none;

	}

	.nbtcs-select {

		top: 27px;

	}

	.nbtcs-w-wrapper .selected {

		padding-left: 3px;

	}

	.vc-blog .blog-content .hb-readmore {

		color: #666;

	}

	.ib-shape .icon i {

		line-height: 65px;

	}

	.comments-area {

		background-color: #fff;

	}

	.nbtcs-w-wrapper .selected {

    		padding-left: 5px;

	}

	.nbtcs-w-wrapper .selected:after {

		margin: 0 0 0 5px;

	}

	@media (min-width: 640px) and (max-width: 768px) {

		.shop_table.cart .actions input.bt-5 {

			float: right;

		}

	}

	@media only screen and (max-width: 768px) {

		body > div#page {

			overflow: hidden;

		}

		.shop_table.cart .cart_item td.product-price, .shop_table.cart .cart_item td.product-quantity {

			padding-right: 30px;

		}

	}

	@media only screen and (max-width: 320px) {

		.middle-section-wrap .middle-right-content .minicart-header .mini-cart-wrap {

			right: -38px !important;

		}

		.header-1 .middle-section-wrap .middle-right-content .header-cart-wrap {

			margin-left: 19px;

		}

		.shop_table.cart td:before {

			position: absolute !important;

			left: 15px !important;

		}

		.shop_table.cart .product-remove {

			text-align: center;

		}

		.shop_table.cart .actions input.bt-5 {

			width: 95%;

		}

	}

	";

	return $css;

}



add_filter('printcart_css_inline', 'homeone_print_embed_style');

add_action( 'after_setup_theme', 'wc_remove_frame_options_header', 11 );

function wc_remove_frame_options_header() {		

	remove_action( 'template_redirect', 'wc_send_frame_options_header' );	

};



function printcart_child_enqueue_styles() {

	wp_enqueue_style('printcart-style', get_template_directory_uri() . '/style.css');

	wp_enqueue_style('printcart-child-style',

		get_stylesheet_directory_uri() . '/style.css',

		array('printcart-style'),

		wp_get_theme()->get('Version')

	);

	wp_enqueue_style('printcart-style-selena', get_stylesheet_directory_uri() . '/selena.css');

	wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/54ed714a8b.js', array(), 'latest', false);
}
add_action('wp_enqueue_scripts', 'printcart_child_enqueue_styles');


function coast_enqueue_styles() {

	$template_url = get_template_directory_uri();



  // Register styles

  wp_register_style( 'abril', 'https://fonts.googleapis.com/css?family=Abril+Fatface', array(), '1.0.0', false );

  wp_register_style( 'nunito-sans', 'https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,800', array(), '1.0.0', false );  

  wp_register_style( 'montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:300,300i', array(), '1.0.0', false );  



  // Enqueue styles

  wp_enqueue_style( 'abril' );

  wp_enqueue_style( 'nunito-sans' );

  wp_enqueue_style( 'montserrat' );

  wp_enqueue_style( 'coastal' );

  wp_enqueue_style( 'coastal-custom' );

}

add_action( 'wp_enqueue_scripts', 'coast_enqueue_styles', 90 );





function coastal_template_single_product_short_description( $post_ID ) {

  $out  = '<div class="product-short-description">';

  $out .= get_the_excerpt();

  // $out .= coastal_short_excerpt(999);

  $out .= '</div>';

  echo $out;

}



// Short Description

add_action( 'coastal_single_product_quick_look', 'coastal_template_single_product_short_description', 10 );





// image product Show

add_action( 'coastal_woocommerce_show_product_images', 'woocommerce_show_product_images', 30 );











remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );





add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

	function woocommerce_output_related_products() {



		$args = array(

			'posts_per_page' => 4,

			'columns'        => 4,

			'orderby'        => 'rand', // @codingStandardsIgnoreLine.

		);



		woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );

	}



/**

 * 

 *

 */

function coastal_woocommerce_template_loop_product_thumbnail() {

  global $post;

  

  // Get the category

  $terms = get_the_terms( $post->ID, 'product_cat' );

  foreach ($terms as $term) {

    $product_cat = $term->name;

    break;

  }



  // Handle Excerpt

  // $short_excerpt = is_front_page() ? coastal_short_excerpt( 12 ) : coastal_short_excerpt( 20 );

  $short_excerpt = coastal_short_excerpt( 12 );



  printf( '

    <span class="et_shop_image">

      %1$s

      <span class="et_offlay">

        <span class="title">

          %3$s

        </span>

      </span>

      <span class="et_overlay">

        <span class="et_overlay_content">

          <span class="category">

            %2$s

          </span>

          <hr />

          <span class="title">

            %3$s

          </span>

          <span class="excerpt">

            %4$s

          </span>

          <span class="et_pb_button">

            %5$s

          </span>

        </span>

      </span>

    </span>',

    woocommerce_get_product_thumbnail(),

    $product_cat,

    $post->post_title,

    $short_excerpt,

    __('View Product', 'woocommerce')

  );

}

add_action( 'woocommerce_before_shop_loop_item_title', 'coastal_woocommerce_template_loop_product_thumbnail', 10 );

/**

 * Coastal Short Excerpt

 *

 */

function coastal_short_excerpt($limit) {

  $excerpt = explode(' ', strip_tags( get_the_excerpt() ), $limit);

  if (count($excerpt)>=$limit) {

    array_pop($excerpt);

    $excerpt = implode(" ",$excerpt).'...';

  } else {

    $excerpt = implode(" ",$excerpt);

  } 

  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);

  return $excerpt;

}

