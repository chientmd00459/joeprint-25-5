<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

get_header( 'shop' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<!-- container fuild single product -->
<div class="banner-wrapper" style="background-image:url(<?php the_field('banner-img');?> );background-image:url(<?php the_field('banner-img');?> ), linear-gradient(to right, #22cbe0, #b7ee5c);  ">


	<div class="banner-img">

		<div class="banner-content">
		<?php
		$page_title = printcart_get_options('show_title_section');
$pd_title = printcart_get_options('nbcore_pd_details_title');
/*  TITLE PRODUCT */
if($page_title) {
	if($pd_title) {
		the_title( '<h1 class="product_title entry-title">', '</h1>' );
	}
} else {
	the_title( '<h1 class="product_title entry-title">', '</h1>' );
}

		 ?>
		 <span class="product-tagline"><?php the_field('product-tagline'); ?></span>
		 </div>


  		<div class="image-single-product">
  			<span class="thumbs-view-more">View More</span>
				<?php
					do_action( 'coastal_woocommerce_show_product_images' );
				?>
			</div>
	</div>

</div>

	<div class="product-quick-look-wrapper shop-main">
	<div class="product-quick-look">
        <div class="single-product-wrap">
        <div class="product-image">
            <?php
            /**
             * woocommerce_before_single_product_summary hook.
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action( 'woocommerce_before_single_product_summary' );
            ?>
        </div>
        <?php
        $nbdesigner_page_design_tool_class = '';
        $class_is_edit_mode = '';
        if(class_exists('Nbdesigner_Plugin') && is_nbdesigner_product($product->get_id())){

            $nbdesigner_page_design_tool = nbdesigner_get_option('nbdesigner_page_design_tool');
            //show design tool in new page
            if($nbdesigner_page_design_tool == 2) {
                $nbdesigner_page_design_tool_class = ' js_open_desginer_in_new_page';
            }

            if ( isset( $_REQUEST['nbo_cart_item_key'] ) && $_REQUEST['nbo_cart_item_key'] != '' ){
                $class_is_edit_mode = ' js_is_edit_mode';
            }
        }

        ?>
        <div class="summary entry-summary<?php echo esc_attr($nbdesigner_page_design_tool_class);?><?php echo esc_attr($class_is_edit_mode);?>">
            <?php
            /**
             * woocommerce_single_product_summary hook.
             *
             * @hooked woocommerce_template_single_title - 5
             * @hooked woocommerce_template_single_rating - 10
             * @hooked woocommerce_template_single_price - 10
             * @hooked woocommerce_template_single_excerpt - 20
             * @hooked woocommerce_template_single_add_to_cart - 30
             * @hooked woocommerce_template_single_meta - 40
             * @hooked woocommerce_template_single_sharing - 50
             * @hooked WC_Structured_Data::generate_product_data() - 60
             */
            do_action( 'woocommerce_single_product_summary' );
            ?>
        </div>
        </div>

<?php
       $enable_price_matrix = get_post_meta($product->get_id(), '_enable_price_matrix', true);
       $settings_modules = !empty(get_option('solutions_core_settings')) ? get_option('solutions_core_settings') : array();
        if(in_array('price-matrix', $settings_modules)) {
            $price_matrix_option = get_option('price-matrix_settings');
            $price_matrix_show_on = $price_matrix_option['wc_price-matrix_show_on'];
        }
        else {
            $price_matrix_show_on = 'default';
        }
        if( ! $product->is_type( 'simple' ) && ($price_matrix_show_on != 'before_tab' || empty($enable_price_matrix) ) ) {
            do_action('netbase_add_to_cart_hook');
        }
        ?>

		<?php $tabs = apply_filters( 'woocommerce_product_tabs', array() ); ?>
		<ul class="tab-nav" role="tablist">
			<li>
				<a href="#details">
					<?php echo __('Product Details', 'woocommerce'); ?>
				</a>
			</li>
			<li>
				<a href="#product-reviews">
					<?php echo __('Product Reviews', 'woocommerce'); ?>
				</a>
			</li>
			<li>
				<a href="#questions">
					<?php echo __('Product Q &amp; A', 'woocommerce'); ?>
				</a>
			</li>
		</ul>


	</div>
	</div>

		<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<div class="product-description">

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

	</div>



</div>
</div>
</div>
</div>
</div>

	<div class="related-products-wrapper">
				<div id="related-products">
			<?php
				/**
				 * woocommerce_after_single_product_summary hook.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary',20);
			?>
		</div>
	</div>




<?php endwhile; // end of the loop. ?>


<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
