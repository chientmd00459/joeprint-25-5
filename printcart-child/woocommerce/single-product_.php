<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<!-- container fuild single product -->
<div class="banner-wrapper" style="background-image:url(<?php the_field('banner-img');?> );background-image:url(<?php the_field('banner-img');?> ), linear-gradient(to right, #22cbe0, #b7ee5c);  ">


	<div class="banner-img">

		<div class="banner-content">
		<?php
		$page_title = printcart_get_options('show_title_section');
$pd_title = printcart_get_options('nbcore_pd_details_title');
/*  TITLE PRODUCT */
if($page_title) {
	if($pd_title) {
		the_title( '<h1 class="product_title entry-title">', '</h1>' );
	}
} else {
	the_title( '<h1 class="product_title entry-title">', '</h1>' );
}

		 ?>
		 <span class="product-tagline"><?php the_field('product-tagline'); ?></span>
		 </div>


  		<div class="image-single-product">
  			<span class="thumbs-view-more">View More</span>
				<?php
					do_action( 'coastal_woocommerce_show_product_images' );
				?>
			</div>
	</div>

</div>

	<div class="product-quick-look-wrapper">
	<div class="product-quick-look">
		<?php
			/**
			 * coastal_single_product_banner hook.
			 *
			 */
			do_action( 'coastal_single_product_quick_look' );
		?>

		<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );

	?>

		<?php $tabs = apply_filters( 'woocommerce_product_tabs', array() ); ?>
		<ul class="tab-nav" role="tablist">
			<li>
				<a href="#details">
					<?php echo __('Product Details', 'woocommerce'); ?>
				</a>
			</li>
			<li>
				<a href="#product-reviews">
					<?php echo __('Product Reviews', 'woocommerce'); ?>
				</a>
			</li>
			<li>
				<a href="#questions">
					<?php echo __('Product Q &amp; A', 'woocommerce'); ?>
				</a>
			</li>
		</ul>


	</div>
	</div>

		<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

	<div class="product-description">

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

	</div>



</div>
</div>
</div>
</div>
</div>

	<div class="related-products-wrapper">
				<div id="related-products">
			<?php
				/**
				 * woocommerce_after_single_product_summary hook.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary',20);
			?>
		</div>
	</div>




<?php endwhile; // end of the loop. ?>


<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
